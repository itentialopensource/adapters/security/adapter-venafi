
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_20:04PM

See merge request itentialopensource/adapters/adapter-venafi!12

---

## 0.3.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-venafi!10

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:12PM

See merge request itentialopensource/adapters/adapter-venafi!9

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:25PM

See merge request itentialopensource/adapters/adapter-venafi!8

---

## 0.3.0 [05-15-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/security/adapter-venafi!7

---

## 0.2.3 [03-27-2024]

* Changes made at 2024.03.27_13:40PM

See merge request itentialopensource/adapters/security/adapter-venafi!6

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_16:13PM

See merge request itentialopensource/adapters/security/adapter-venafi!5

---

## 0.2.1 [02-27-2024]

* Changes made at 2024.02.27_11:47AM

See merge request itentialopensource/adapters/security/adapter-venafi!4

---

## 0.2.0 [01-02-2024]

* Adapter Migration

See merge request itentialopensource/adapters/security/adapter-venafi!3

---

## 0.1.3 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-venafi!1

---

## 0.1.2 [06-08-2022]

* Bug fixes and performance improvements

See commit a11d092

---

## 0.1.1 [06-07-2022]

* Bug fixes and performance improvements

See commit ca5de82

---
