openapi: 3.0.0
info:
  title: Venafi as a Service API for Edge Management Service Operations
  description: API for Venafi as a Service
  contact: {}
  version: 'v1'
servers:
- url: https://{defaultHost}
  variables:
    defaultHost:
      default: www.example.com
paths:
  /v1/pairingcodes:
    get:
      tags:
      - Edge Hub
      summary: pairingcodes_getAll
      description: Retrieve list of all pairing codes used by edge instances for company
      operationId: pairingcodes_getAll
      parameters: []
      responses:
        '200':
          description: All Pairing Codes
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PairingCodeResponse'
      deprecated: false
      security:
      - tppl-api-key: []
    post:
      tags:
      - Edge Hub
      summary: pairingcodes_create
      description: Generate new pairing code for edge instance registration
      operationId: pairingcodes_create
      parameters: []
      requestBody:
        description: ''
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PairingCodeRequest'
        required: false
      responses:
        '201':
          description: Pairing Code Created
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PairingCodeInformation'
        '400':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      deprecated: false
      security:
      - tppl-api-key: []
  /v1/edgeinstances/{id}:
    get:
      tags:
      - Edge Hub
      summary: edgeinstances_getById
      description: Retrieve edge instance with 'id' attribute matching specified value; specified value should be uuid not numeric
      operationId: edgeinstances_getById
      parameters:
      - name: id
        in: path
        description: EdgeInstance ID
        required: true
        style: simple
        schema:
          type: string
          format: uuid
      - name: statusDetails
        in: query
        description: ''
        style: form
        explode: true
        schema:
          type: boolean
          default: false
      responses:
        '200':
          description: Edge instance matching specified id
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EdgeInstanceInformation'
        '404':
          description: Edge instance not found
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '400':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '412':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      deprecated: false
      security:
      - tppl-api-key: []
    put:
      tags:
      - Edge Hub
      summary: edgeinstances_update
      description: Update edge instance with specified new value
      operationId: edgeinstances_update
      parameters:
      - name: id
        in: path
        description: EdgeInstance ID
        required: true
        style: simple
        schema:
          type: string
          format: uuid
      requestBody:
        description: ''
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/EdgeInstanceRequest'
        required: false
      responses:
        '200':
          description: Edge Instance Updated
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EdgeInstanceInformation'
        '404':
          description: Edge Instance Not Found
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '400':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '412':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      deprecated: false
      security:
      - tppl-api-key: []
    delete:
      tags:
      - Edge Hub
      summary: edgeinstances_delete
      description: Delete edge instance
      operationId: edgeinstances_delete
      parameters:
      - name: id
        in: path
        description: EdgeInstance ID
        required: true
        style: simple
        schema:
          type: string
          format: uuid
      responses:
        '204':
          description: Edge Instance Deleted
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EdgeInstanceDeleteResponse'
        '404':
          description: Edge Instance Not Found
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '400':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '412':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      deprecated: false
      security:
      - tppl-api-key: []
  /v1/edgeinstances:
    get:
      tags:
      - Edge Hub
      summary: edgeinstances_getAll
      description: Retrieve list of all edge instances for company
      operationId: edgeinstances_getAll
      parameters:
      - name: environmentId
        in: query
        description: ''
        style: form
        explode: true
        schema:
          type: string
          format: uuid
      responses:
        '200':
          description: Edge instances matching specified id
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EdgeInstanceResponse'
      deprecated: false
      security:
      - tppl-api-key: []
  /v1/edgeencryptionkeys:
    get:
      tags:
      - Edge Hub
      summary: edgeencryptionkeys_getAll
      description: Retrieve the list of edge encryption keys based on query parameters
      operationId: edgeencryptionkeys_getAll
      parameters:
      - name: edgeInstanceId
        in: query
        description: ''
        style: form
        explode: true
        schema:
          type: string
          format: uuid
      responses:
        '200':
          description: List of Edge Encryption keys
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EncryptionKeysResponse'
      deprecated: false
      security:
      - tppl-api-key: []
  /v1/edgeencryptionkeys/{id}:
    get:
      tags:
      - Edge Hub
      summary: edgeencryptionkeys_getById
      description: Retrieve edge encryption key with 'id' attribute matching specified value; specified value should be string not numeric
      operationId: edgeencryptionkeys_getById
      parameters:
      - name: id
        in: path
        description: ''
        required: true
        style: simple
        schema:
          type: string
      responses:
        '200':
          description: Encryption key information
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EncryptionKeyInformation'
        '404':
          description: Encryption key not found
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '400':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '412':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      deprecated: false
      security:
      - tppl-api-key: []
  /v1/billofmaterials:
    get:
      tags:
      - Edge Hub
      summary: billofmaterials_getLatest
      description: Retrieve list of latest bills of materials
      operationId: billofmaterials_getLatest
      parameters: []
      responses:
        '200':
          description: Bills of materials
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BillOfMaterialResponse'
      deprecated: false
      security:
      - tppl-api-key: []
  /v1/edgeworkers/{id}:
    get:
      tags:
      - Edge Worker
      summary: edgeinstances_getById_1
      description: ''
      operationId: edgeinstances_getById_1
      parameters:
      - name: id
        in: path
        description: EdgeWorker ID
        required: true
        style: simple
        schema:
          type: string
          format: uuid
      responses:
        '200':
          description: Edge worker information
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EdgeWorkerInformation'
        '404':
          description: Edge worker not found
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '400':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '412':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      deprecated: false
      security:
      - tppl-api-key: []
    delete:
      tags:
      - Edge Worker
      summary: edgeworker_delete
      description: Delete edge worker
      operationId: edgeworker_delete
      parameters:
      - name: id
        in: path
        description: EdgeWorker ID
        required: true
        style: simple
        schema:
          type: string
          format: uuid
      responses:
        '404':
          description: Edge worker not found
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '400':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '412':
          description: Request Conditions Failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      deprecated: false
      security:
      - tppl-api-key: []
  /v1/edgeworkers:
    get:
      tags:
      - Edge Worker
      summary: edgeworkers_getAll
      description: Retrieve list of all edge workers for company
      operationId: edgeworkers_getAll
      parameters:
      - name: edgeInstanceId
        in: query
        description: ''
        style: form
        explode: true
        schema:
          type: string
          format: uuid
      responses:
        '200':
          description: List of Edge workers
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EdgeWorkersResponse'
      deprecated: false
      security:
      - tppl-api-key: []
    post:
      tags:
      - Edge Worker
      summary: edgeworkers_create
      description: Create edge worker and generate the pairing information
      operationId: edgeworkers_create
      parameters: []
      requestBody:
        description: ''
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/EdgeWorkerRequest'
        required: false
      responses:
        '201':
          description: Edge worker information
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EdgeWorkerInformation'
        '400':
          description: Request conditions failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '412':
          description: Request conditions failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      deprecated: false
      security:
      - tppl-api-key: []
  /v1/edgeworkers/{id}/pair:
    post:
      tags:
      - Edge Worker
      summary: edgeworkers_pair
      description: ''
      operationId: edgeworkers_pair
      parameters:
      - name: id
        in: path
        description: EdgeWorker ID
        required: true
        style: simple
        schema:
          type: string
          format: uuid
      requestBody:
        description: ''
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/EdgeWorkerRequest'
        required: false
      responses:
        '200':
          description: Edge worker paired with Edge hub
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EdgeWorkerInformation'
        '400':
          description: Request conditions failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '412':
          description: Request conditions failed
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      deprecated: false
      security:
      - tppl-api-key: []
components:
  schemas:
    PairingCodeInformation:
      title: PairingCodeInformation
      type: object
      properties:
        id:
          type: string
          format: uuid
        companyId:
          type: string
          format: uuid
        productEntitlements:
          type: array
          items:
            $ref: '#/components/schemas/ProductEntitlement'
          description: ''
        environmentId:
          type: string
          format: uuid
        pairingCode:
          type: string
        reuseCount:
          type: integer
          format: int32
        expirationDate:
          type: string
          format: date-time
        modificationDate:
          type: string
          format: date-time
    ErrorInformation:
      title: ErrorInformation
      type: object
      properties:
        code:
          type: integer
          format: int32
        message:
          type: string
        args:
          type: array
          items:
            type: object
          description: ''
    ErrorResponse:
      title: ErrorResponse
      type: object
      properties:
        errors:
          type: array
          items:
            $ref: '#/components/schemas/ErrorInformation'
          description: ''
    PairingCodeRequest:
      title: PairingCodeRequest
      required:
      - environmentId
      - expirationDate
      type: object
      properties:
        environmentId:
          type: string
          format: uuid
        reuseCount:
          type: integer
          format: int32
        expirationDate:
          type: string
          format: date-time
    PairingCodeResponse:
      title: PairingCodeResponse
      type: object
      properties:
        pairingCodes:
          type: array
          items:
            $ref: '#/components/schemas/PairingCodeInformation'
          description: ''
    ChartDetails:
      title: ChartDetails
      type: object
      properties:
        name:
          type: string
        version:
          type: string
    EdgeInstanceConnectionDetails:
      title: EdgeInstanceConnectionDetails
      type: object
      properties:
        edgeInstanceId:
          type: string
          format: uuid
        timestamp:
          type: string
          format: date-time
        lastConnectedDate:
          type: string
          format: date-time
        lastDisconnectedDate:
          type: string
          format: date-time
        activeMessageCount:
          type: integer
          format: int32
        expiredMessageCount:
          type: integer
          format: int32
        failedMessageCount:
          type: integer
          format: int32
    EdgeInstanceHealthDetails:
      title: EdgeInstanceHealthDetails
      type: object
      properties:
        edgeInstanceId:
          type: string
          format: uuid
        timestamp:
          type: string
          format: date-time
        systemstatus:
          type: array
          items:
            $ref: '#/components/schemas/NamespaceStatusDetails'
          description: ''
        installedCharts:
          type: array
          items:
            $ref: '#/components/schemas/NamespaceChartDetails'
          description: ''
    EdgeInstanceInformation:
      title: EdgeInstanceInformation
      type: object
      properties:
        id:
          type: string
          format: uuid
        companyId:
          type: string
          format: uuid
        productEntitlements:
          type: array
          items:
            $ref: '#/components/schemas/ProductEntitlement'
          description: ''
        environmentId:
          type: string
          format: uuid
        pairingCodeId:
          type: string
          format: uuid
        name:
          type: string
        edgeType:
          $ref: '#/components/schemas/EdgeType'
        edgeStatus:
          $ref: '#/components/schemas/EdgeStatus'
        clientId:
          type: string
        modificationDate:
          type: string
          format: date-time
        address:
          type: string
        deploymentDate:
          type: string
          format: date-time
        lastSeenOnDate:
          type: string
          format: date-time
        statusDetails:
          $ref: '#/components/schemas/EdgeInstanceStatusDetailsInformation'
        workerStatusDetails:
          type: array
          items:
            $ref: '#/components/schemas/EdgeWorkerStatusDetailsInformation'
          description: ''
        encryptionKeyId:
          type: string
        encryptionKeyDeploymentDate:
          type: string
          format: date-time
        integrationServicesCount:
          type: integer
          format: int64
    EdgeInstanceStatusDetailsInformation:
      title: EdgeInstanceStatusDetailsInformation
      type: object
      properties:
        connectionDetails:
          type: array
          items:
            $ref: '#/components/schemas/EdgeInstanceConnectionDetails'
          description: ''
        healthDetails:
          type: array
          items:
            $ref: '#/components/schemas/EdgeInstanceHealthDetails'
          description: ''
    EdgeWorkerHealthDetails:
      title: EdgeWorkerHealthDetails
      type: object
      properties:
        edgeWorkerId:
          type: string
          format: uuid
        edgeInstanceId:
          type: string
          format: uuid
        timestamp:
          type: string
          format: date-time
        host:
          type: string
        port:
          type: integer
          format: int32
        status:
          type: string
        age:
          type: integer
          format: int64
        cpuUsage:
          type: integer
          format: int32
        memoryUsage:
          type: integer
          format: int64
        services:
          type: array
          items:
            $ref: '#/components/schemas/WorkerServiceStatusDetails'
          description: ''
    EdgeWorkerStatusDetailsInformation:
      title: EdgeWorkerStatusDetailsInformation
      type: object
      properties:
        edgeWorkerId:
          type: string
          format: uuid
        healthDetails:
          type: array
          items:
            $ref: '#/components/schemas/EdgeWorkerHealthDetails'
          description: ''
    NamespaceChartDetails:
      title: NamespaceChartDetails
      type: object
      properties:
        name:
          type: string
        charts:
          type: array
          items:
            $ref: '#/components/schemas/ChartDetails'
          description: ''
    NamespaceStatusDetails:
      title: NamespaceStatusDetails
      type: object
      properties:
        timestamp:
          type: string
          format: date-time
        name:
          type: string
        pods:
          type: array
          items:
            $ref: '#/components/schemas/PodStatusDetails'
          description: ''
    PodStatusDetails:
      title: PodStatusDetails
      type: object
      properties:
        name:
          type: string
        age:
          type: integer
          format: int64
        restartCount:
          type: integer
          format: int32
        status:
          type: string
        cpuUsage:
          type: integer
          format: int32
        memoryUsage:
          type: integer
          format: int64
    WorkerServiceStatusDetails:
      title: WorkerServiceStatusDetails
      type: object
      properties:
        name:
          type: string
        status:
          type: string
    EdgeInstanceRequest:
      title: EdgeInstanceRequest
      required:
      - name
      type: object
      properties:
        name:
          type: string
    EdgeInstanceDeleteResponse:
      title: EdgeInstanceDeleteResponse
      type: object
      properties:
        name:
          type: string
    EdgeInstanceResponse:
      title: EdgeInstanceResponse
      type: object
      properties:
        edgeInstances:
          type: array
          items:
            $ref: '#/components/schemas/EdgeInstanceInformation'
          description: ''
    EncryptionKeyInformation:
      title: EncryptionKeyInformation
      type: object
      properties:
        id:
          type: string
        companyId:
          type: string
          format: uuid
        key:
          type: string
        keyAlgorithm:
          $ref: '#/components/schemas/KeyAlgorithm'
        lastBackupDate:
          type: string
          format: date-time
    EncryptionKeysResponse:
      title: EncryptionKeysResponse
      type: object
      properties:
        encryptionKeys:
          type: array
          items:
            $ref: '#/components/schemas/EncryptionKeyInformation'
          description: ''
    EdgeWorkerInformation:
      title: EdgeWorkerInformation
      type: object
      properties:
        id:
          type: string
          format: uuid
        companyId:
          type: string
          format: uuid
        host:
          type: string
        port:
          type: integer
          format: int32
        pairingCode:
          type: string
        pairingPublicKey:
          type: string
        edgeInstanceId:
          type: string
          format: uuid
        environmentId:
          type: string
          format: uuid
        status:
          $ref: '#/components/schemas/Status'
        lastSeenOnDate:
          type: string
          format: date-time
    EdgeWorkerRequest:
      title: EdgeWorkerRequest
      type: object
      properties:
        host:
          type: string
        port:
          type: integer
          format: int32
        edgeInstanceId:
          type: string
          format: uuid
    EdgeWorkersResponse:
      title: EdgeWorkersResponse
      type: object
      properties:
        edgeWorkers:
          type: array
          items:
            $ref: '#/components/schemas/EdgeWorkerInformation'
          description: ''
    BillOfMaterialResponse:
      title: BillOfMaterialResponse
      type: object
      properties:
        billsOfMaterials:
          type: array
          items:
            $ref: '#/components/schemas/BillOfMaterialsInformation'
          description: ''
    BillOfMaterialsInformation:
      title: BillOfMaterialsInformation
      type: object
      properties:
        name:
          type: string
        version:
          type: string
        charts:
          type: array
          items:
            $ref: '#/components/schemas/Chart'
          description: ''
    Chart:
      title: Chart
      type: object
      properties:
        name:
          type: string
        version:
          type: string
        url:
          type: string
        signature:
          type: string
    EdgeStatus:
      title: EdgeStatus
      enum:
      - ACTIVE
      - LOST_CONNECTION
      - REGISTERED
      - PAIRED
      - LOST_CONNECTION_DURING_INSTALL
      - INSTALLED
      - INSTALL_FAILED
      - UNHEALTHY
      - ERROR
      - INSTALLING
      type: string
    EdgeType:
      title: EdgeType
      enum:
      - ALL
      - HUB
      - SATELLITE
      type: string
    KeyAlgorithm:
      title: KeyAlgorithm
      enum:
      - RSA
      - ED25519
      type: string
    ProductEntitlement:
      title: ProductEntitlement
      enum:
      - ANY
      - MIRA
      - DEVOPS
      - OUTAGE_DETECTION
      type: string
    Status:
      title: Status
      enum:
      - DRAFT
      - PAIRED
      - ACTIVE
      - FAILED
      - INACTIVE
      type: string
  securitySchemes:
    tppl-api-key:
      type: apiKey
      name: tppl-api-key
      in: header
security: []
tags:
- name: Edge Hub
  description: ''
- name: Edge Worker
  description: ''
