# Venafi

Vendor: Venafi
Homepage: https://venafi.com/

Product: Venafi
Product Page: https://venafi.com/

## Introduction
We classify Venafi into the Security domain as Venafi provides security-centric functionalities for managing digital keys and certificates, ensuring compliance with security standards and automating security workflows.

"Venafi designed the Control Plane from the ground up to provide the highest levels of security" 

## Why Integrate
The Venafi adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Venafi to address diverse security challenges.

With this adapter you have the ability to perform operations with Venafi such as:

- Certificates
- Key Management

## Additional Product Documentation
The [API documents for Venafi](https://developer.venafi.com/tlsprotectcloud/docs/welcome/)