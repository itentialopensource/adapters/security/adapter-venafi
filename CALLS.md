## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Venafi. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Venafi.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Venafi. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">applicationsGetById(id, ownershipTree, callback)</td>
    <td style="padding:15px">Retrieve Application</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsUpdate(id, body, callback)</td>
    <td style="padding:15px">Update Application</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsDelete(id, callback)</td>
    <td style="padding:15px">Delete Application</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsGetByName(name, ownershipTree, callback)</td>
    <td style="padding:15px">Retrieve Application By Name</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsGetAll(ownerDetails, ownershipCheck, issuingTemplateAssigned, ownershipTree, callback)</td>
    <td style="padding:15px">Retrieve Applications</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsCreate(body, callback)</td>
    <td style="padding:15px">Create Application</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsGetScanafiConfigById(id, callback)</td>
    <td style="padding:15px">Retrieve Scanafi Configuration for Application by id</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications/{pathv1}/scanaficonfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsInvitationsCreate(id, body, callback)</td>
    <td style="padding:15px">Create User Invitation To an Application</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications/{pathv1}/invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateissuingtemplatesGetByAlias(name, alias, callback)</td>
    <td style="padding:15px">Retrieve Certificate Issuing Template By Application Name and Template Alias</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications/{pathv1}/certificateissuingtemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsAssignToCertificates(body, callback)</td>
    <td style="padding:15px">Replace | Add | Delete Application(s) to Certificate(s) Specified by their ids</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applications/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatesValidation(body, callback)</td>
    <td style="padding:15px">Submit certificates for validation</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatesGetAllAsCsv(subject, limit, callback)</td>
    <td style="padding:15px">Retrieve Certificates</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateimportsCreate(body, callback)</td>
    <td style="padding:15px">Import Certificate With Issuer Certificates(optional)</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatesGetById(ownershipTree, id, callback)</td>
    <td style="padding:15px">Retrieve Certificate By Id</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateretirementRetireCertificates(body, callback)</td>
    <td style="padding:15px">Retire the specified certificates for company</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates/retirement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateretirementRecoverCertificates(body, callback)</td>
    <td style="padding:15px">Recover the specified certificates for company</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates/recovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateretirementDeleteCertificates(body, callback)</td>
    <td style="padding:15px">Delete the specified retired certificates for company</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates/deletion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatesGetContentsById(id, format = 'PEM', chainOrder = 'EE_ONLY', callback)</td>
    <td style="padding:15px">Retrieve Certificate(s) (bytes) By Id</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates/{pathv1}/contents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatesGetByManagedCertificateId(ownershipTree, id, callback)</td>
    <td style="padding:15px">Retrieve Certificate By Managed Certificate Id</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates/managed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatesDownloadkeystoreById(id, body, callback)</td>
    <td style="padding:15px">Download Keystore By Certificate Id</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificates/{pathv1}/keystore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatesSearchGetByExpressionAsCsv(body, callback)</td>
    <td style="padding:15px">Retrieve Certificates Matching Search Criteria</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificatesearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateinstancesValidation(body, callback)</td>
    <td style="padding:15px">Submit certificate instances for validation</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificateinstances/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateinstancesGetAllAsCsv(source = 'UNKNOWN', ipAddress, hostname, limit, callback)</td>
    <td style="padding:15px">Retrieve Certificate Instances</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificateinstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateinstancesGetById(id, callback)</td>
    <td style="padding:15px">Retrieve Certificate Instance By Id</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificateinstances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateinstancesSearchGetByExpressionAsCsv(body, callback)</td>
    <td style="padding:15px">Retrieve Certificate Instances Matching Search Criteria</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificateinstancesearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">savedSearchGetById(id, callback)</td>
    <td style="padding:15px">Retrieve saved search</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/savedsearches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">savedSearchUpdate(id, body, callback)</td>
    <td style="padding:15px">Update Saved Search</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/savedsearches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">savedSearchDelete(id, callback)</td>
    <td style="padding:15px">Delete Saved Search</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/savedsearches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">savedSearchGetAll(callback)</td>
    <td style="padding:15px">Retrieve Saved Searches</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/savedsearches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveSearchCreate(body, callback)</td>
    <td style="padding:15px">Create saved search</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/savedsearches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateaggregatesGetUnassignedCertificateAggregates(callback)</td>
    <td style="padding:15px">Retrieve Expiration Aggregations for Unassigned Certificates</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificateaggregates/unassigned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateaggregatesGetCertificateAggregatesForRange(callback)</td>
    <td style="padding:15px">Retrieve Expiration Aggregations for Specific Timerange</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificateaggregates/range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateaggregatesGetCertificateAggregations(body, callback)</td>
    <td style="padding:15px">Retrieve Aggregates for Company Certificates</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificateaggregates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateaggregatesGetCertificateInstallationsQuota(callback)</td>
    <td style="padding:15px">Retrieve Certificate Installations Usage</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificateaggregates/quota?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateaggregatesGetCertificateAggregates(name = 'application', callback)</td>
    <td style="padding:15px">Retrieve Aggregations for Certificate Expiration</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificateaggregates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificaterequestsGetAll(callback)</td>
    <td style="padding:15px">Retrieve Certificate Requests</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificaterequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificaterequestsCreate(body, callback)</td>
    <td style="padding:15px">Create Certificate Request</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificaterequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificaterequestsValidation(body, callback)</td>
    <td style="padding:15px">Validate Certificate Request</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificaterequests/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificaterequestsGetById(id, callback)</td>
    <td style="padding:15px">Retrieve Certificate Request By Id</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificaterequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificaterequestsResubmitById(id, body, callback)</td>
    <td style="padding:15px">Resubmit a Certificate Request By Id</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificaterequests/{pathv1}/resubmission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getByExpression(body, callback)</td>
    <td style="padding:15px">Retrieve Certificate Requests Matching Search Criteria</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/certificaterequestssearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationservertypesGetById(id, callback)</td>
    <td style="padding:15px">Retrieve Application Server Type by id</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applicationservertypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationservertypesDelete(id, callback)</td>
    <td style="padding:15px">Delete Application Server Type</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applicationservertypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationservertypesGetAll(callback)</td>
    <td style="padding:15px">Retrieve Application Server Types</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applicationservertypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationservertypesCreate(body, callback)</td>
    <td style="padding:15px">Create Application Server Type</td>
    <td style="padding:15px">{base_path}/outagedetection/{version}/applicationservertypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">servicesaggregatesGet(callback)</td>
    <td style="padding:15px">Retrieve Aggregations for Integrations Services for a company</td>
    <td style="padding:15px">{base_path}/{version}/integrationservicesaggregates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">integrationsservicesGetById(id, callback)</td>
    <td style="padding:15px">Retrieve an Integration Service By Id</td>
    <td style="padding:15px">{base_path}/{version}/integrationservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">integrationsservicesDelete(id, retireCertificates, callback)</td>
    <td style="padding:15px">Delete an Integrations Service</td>
    <td style="padding:15px">{base_path}/{version}/integrationservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">integrationsservicesUpdate(id, body, callback)</td>
    <td style="padding:15px">Update Service properties</td>
    <td style="padding:15px">{base_path}/{version}/integrationservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">integrationsservicesGetAll(totalCount, edgeInstanceId, callback)</td>
    <td style="padding:15px">Retrieve all Integration Services for a company</td>
    <td style="padding:15px">{base_path}/{version}/integrationservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">integrationsservicesCreate(body, callback)</td>
    <td style="padding:15px">Create an Integration Service for an Environment</td>
    <td style="padding:15px">{base_path}/{version}/integrationservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">integrationsEnvironmentsGet(name, callback)</td>
    <td style="padding:15px">Retrieve Environments</td>
    <td style="padding:15px">{base_path}/{version}/environments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">integrationsEnvironmentsGet1(id, callback)</td>
    <td style="padding:15px">Retrieve Environment</td>
    <td style="padding:15px">{base_path}/{version}/environments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pairingcodesGetAll(callback)</td>
    <td style="padding:15px">Retrieve Pairing Codes for Edge Instances</td>
    <td style="padding:15px">{base_path}/{version}/pairingcodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pairingcodesCreate(body, callback)</td>
    <td style="padding:15px">Create Pairing Code for Edge Instance</td>
    <td style="padding:15px">{base_path}/{version}/pairingcodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeinstancesGetById(id, statusDetails, callback)</td>
    <td style="padding:15px">Retrieve Edge Instance By Id</td>
    <td style="padding:15px">{base_path}/{version}/edgeinstances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeinstancesUpdate(id, body, callback)</td>
    <td style="padding:15px">Update Edge Instance</td>
    <td style="padding:15px">{base_path}/{version}/edgeinstances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeinstancesDelete(id, callback)</td>
    <td style="padding:15px">Delete Edge Instance</td>
    <td style="padding:15px">{base_path}/{version}/edgeinstances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeinstancesGetAll(environmentId, callback)</td>
    <td style="padding:15px">Retrieve Edge Instances</td>
    <td style="padding:15px">{base_path}/{version}/edgeinstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeencryptionkeysGetAll(edgeInstanceId, callback)</td>
    <td style="padding:15px">Retrieve Edge Encryption Keys</td>
    <td style="padding:15px">{base_path}/{version}/edgeencryptionkeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeencryptionkeysGetById(id, callback)</td>
    <td style="padding:15px">Retrieve Edge Encryption Key By Id</td>
    <td style="padding:15px">{base_path}/{version}/edgeencryptionkeys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">billofmaterialsGetLatest(callback)</td>
    <td style="padding:15px">Retrieve latest bills of materials</td>
    <td style="padding:15px">{base_path}/{version}/billofmaterials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeinstancesGetById1(id, callback)</td>
    <td style="padding:15px">Retrieve Edge Worker By Id</td>
    <td style="padding:15px">{base_path}/{version}/edgeworkers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeworkerDelete(id, callback)</td>
    <td style="padding:15px">Delete Edge Worker</td>
    <td style="padding:15px">{base_path}/{version}/edgeworkers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeworkersGetAll(edgeInstanceId, callback)</td>
    <td style="padding:15px">Retrieve Edge Workers</td>
    <td style="padding:15px">{base_path}/{version}/edgeworkers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeworkersCreate(body, callback)</td>
    <td style="padding:15px">Create Edge Worker</td>
    <td style="padding:15px">{base_path}/{version}/edgeworkers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edgeworkersPair(id, body, callback)</td>
    <td style="padding:15px">Pair Edge Worker with Edge Instance</td>
    <td style="padding:15px">{base_path}/{version}/edgeworkers/{pathv1}/pair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activitylogsGetAllByExpressionAsJson(body, callback)</td>
    <td style="padding:15px">Export Activity Log Entries Matching Search Criteria as JSON text</td>
    <td style="padding:15px">{base_path}/{version}/activitylogsearch/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activitylogsGetByExpression(body, callback)</td>
    <td style="padding:15px">Retrieve Activity Log Entries Matching Search Criteria</td>
    <td style="padding:15px">{base_path}/{version}/activitylogsearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activitylogtypesGet(callback)</td>
    <td style="padding:15px">Retrieve types of activities used for event logging</td>
    <td style="padding:15px">{base_path}/{version}/activitylogtypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersGetById(id, callback)</td>
    <td style="padding:15px">Retrieve User By Id</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersDelete(id, callback)</td>
    <td style="padding:15px">Delete User</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersGetAll(userStatus, username, callback)</td>
    <td style="padding:15px">Retrieve All Users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersGetByUsername(username, callback)</td>
    <td style="padding:15px">Retrieve User By Username</td>
    <td style="padding:15px">{base_path}/{version}/users/username/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersGetUserLoginConfig(username, callback)</td>
    <td style="padding:15px">Check if the username belongs to an SSO account</td>
    <td style="padding:15px">{base_path}/{version}/users/username/{pathv1}/loginconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUpdateRoles(id, body, callback)</td>
    <td style="padding:15px">Update Roles for User</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUpdateAccountType(id, body, callback)</td>
    <td style="padding:15px">Update Account Type for User</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/accounttype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUpdateLocalLogin(id, body, callback)</td>
    <td style="padding:15px">Enable/disable local login for user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/locallogin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apikeyGetByKey(userId, key, body, callback)</td>
    <td style="padding:15px">Replace ApiKey for User</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/apikeys/{pathv2}/replacement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apikeyGetAll(userId, apiKeyStatus, callback)</td>
    <td style="padding:15px">Retrieve ApiKeys for User</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/apikeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apikeyCreate(userId, body, callback)</td>
    <td style="padding:15px">Create ApiKey for User</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/apikeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apikeyGetByKey1(userId, key, callback)</td>
    <td style="padding:15px">Retrieve ApiKey By Key</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/apikeys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apikeyGetByKey2(userId, key, body, callback)</td>
    <td style="padding:15px">Request rotation of the current active ApiKey for User</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/apikeys/{pathv2}/rotationrequest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apikeyGetByKey3(userId, key, callback)</td>
    <td style="padding:15px">Finalize the ApiKey rotation for User</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/apikeys/{pathv2}/rotation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">useraccountsGetByAuth(callback)</td>
    <td style="padding:15px">Retrieve User Account Information</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">useraccountsCreate(body, callback)</td>
    <td style="padding:15px">Create User Account</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">useraccountsActivate(k, v, callback)</td>
    <td style="padding:15px">Activate User Account</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/activation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">useraccountsRotateApiKey(k, v, callback)</td>
    <td style="padding:15px">Rotate Api Key for User Account</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/apikeyrotation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">useraccountsResendActivation(body, callback)</td>
    <td style="padding:15px">Resend the user activation email</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/activationresend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">useraccountsCheckResetPasswordToken(token, callback)</td>
    <td style="padding:15px">Check if the reset password token is valid</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/passwordreset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">useraccountsResetPassword(body, callback)</td>
    <td style="padding:15px">Reset the password for a user</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/passwordreset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">useraccountsUpdatePassword(body, callback)</td>
    <td style="padding:15px">Update the password for a user</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/updatepassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">useraccountsResetPassword1(body, callback)</td>
    <td style="padding:15px">Change password for user</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invitationsGetById(id, callback)</td>
    <td style="padding:15px">invitations_getById</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invitationsConfirm(body, callback)</td>
    <td style="padding:15px">invitations_confirm</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invitationsCreate(body, callback)</td>
    <td style="padding:15px">Create User Invitation</td>
    <td style="padding:15px">{base_path}/{version}/useraccounts/invitations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">preferencesGetById(id, callback)</td>
    <td style="padding:15px">Retrieve User Preference By Id</td>
    <td style="padding:15px">{base_path}/{version}/preferences/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">preferencesUpdate(id, body, callback)</td>
    <td style="padding:15px">Update User Preference</td>
    <td style="padding:15px">{base_path}/{version}/preferences/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">preferencesDelete(id, callback)</td>
    <td style="padding:15px">Delete User Preference</td>
    <td style="padding:15px">{base_path}/{version}/preferences/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">preferencesGetByName(name = 'DASHBOARD_PERSONA', callback)</td>
    <td style="padding:15px">Retrieve User Preference By Name</td>
    <td style="padding:15px">{base_path}/{version}/preferences/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">preferencesGetAll(callback)</td>
    <td style="padding:15px">Retrieve User Preferences</td>
    <td style="padding:15px">{base_path}/{version}/preferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">preferencesCreate(body, callback)</td>
    <td style="padding:15px">Create User Preference</td>
    <td style="padding:15px">{base_path}/{version}/preferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationsGetById(id, callback)</td>
    <td style="padding:15px">Retrieve Notification Configuration By Id</td>
    <td style="padding:15px">{base_path}/{version}/notifications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationsUpdate(id, body, callback)</td>
    <td style="padding:15px">Notification Configuration</td>
    <td style="padding:15px">{base_path}/{version}/notifications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationsDelete(id, callback)</td>
    <td style="padding:15px">Delete NotificationConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/notifications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationsGetAll(callback)</td>
    <td style="padding:15px">Retrieve Notification Configurations</td>
    <td style="padding:15px">{base_path}/{version}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationsCreate(body, callback)</td>
    <td style="padding:15px">Create Notification Configuration</td>
    <td style="padding:15px">{base_path}/{version}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationsGetByType(type = 'CERTIFICATE_EXPIRATION', callback)</td>
    <td style="padding:15px">Retrieve Notification Configuration By Type</td>
    <td style="padding:15px">{base_path}/{version}/notifications/type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationsUnsubscribe(id, recipientToken, callback)</td>
    <td style="padding:15px">Unsubscribe recipient from receiving email notification</td>
    <td style="padding:15px">{base_path}/{version}/notifications/{pathv1}/unsubscribe/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rotate(callback)</td>
    <td style="padding:15px">Rotates Company Data Encryption Key</td>
    <td style="padding:15px">{base_path}/{version}/dataencryptionkeys/rotation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(callback)</td>
    <td style="padding:15px">Retrieve Single Sign On Configurations</td>
    <td style="padding:15px">{base_path}/{version}/ssoconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create(body, callback)</td>
    <td style="padding:15px">Create Single Sign On Configuration</td>
    <td style="padding:15px">{base_path}/{version}/ssoconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(id, body, callback)</td>
    <td style="padding:15px">Update Single Sign On Configuration</td>
    <td style="padding:15px">{base_path}/{version}/ssoconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(id, callback)</td>
    <td style="padding:15px">Delete Single Sign On Configuration</td>
    <td style="padding:15px">{base_path}/{version}/ssoconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get1(id, callback)</td>
    <td style="padding:15px">Retrieve a Team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete1(id, callback)</td>
    <td style="padding:15px">delete a team</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patch(id, body, callback)</td>
    <td style="padding:15px">Update Team properties</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get2(callback)</td>
    <td style="padding:15px">Retrieve teams for the current company</td>
    <td style="padding:15px">{base_path}/{version}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create1(body, callback)</td>
    <td style="padding:15px">Create Team</td>
    <td style="padding:15px">{base_path}/{version}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOwner(id, body, callback)</td>
    <td style="padding:15px">Add team owners</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/owners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeOwner(id, body, callback)</td>
    <td style="padding:15px">Remove team owners</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/owners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMember(id, body, callback)</td>
    <td style="padding:15px">Add team members</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeMember(id, body, callback)</td>
    <td style="padding:15px">Remove team members</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
